Question 1

package Javaexe;

public abstract class Arithmetic {

 int num1,num2,num3;
	 
	 public Arithmetic() {
		super();
	}

	public void read(int num1,int num2) {
		 this.num1=num1;
		 this.num2=num2;
	 }
	
	 public abstract void calculate();
		 
	 
	public void display() {
		System.out.println("Num 1= "+num1+" "+"Num 2= "+num2);
	}

}

_____________________________________________________________________________________________________________________________________________________

package Javaexe;

public class Addition extends Arithmetic {
		
	public void calculate() {
		num3=num1+num2;
		System.out.println("Addition= "+num3);
	}

}

_________________________________________________________________________________________________________________________________________________________

package Javaexe;

public class Subtraction extends Arithmetic {

	
	public void calculate() {
		num3=num1-num2;
		System.out.println("Subtraction= "+num3);
	}
}

____________________________________________________________________________________________________________________________________________________

package Javaexe;

public class Multiplication extends Arithmetic {	
	@Override
	public void calculate() {
		num3=num1*num2;
		System.out.println("Multiplication= "+num3);
	}

}

________________________________________________________________________________________________________________________________________________________
package Javaexe;

public class Division extends Arithmetic {
	
	public void calculate () {
		try {
			num3=num1/num2;
			System.out.println("Division= "+num3);
			}
			catch(Exception e){
				
			}			System.out.println("Cannot Divide by 0");
	
		
	}
}

_________________________________________________________________________________________________________________________________________________________________________

package Javaexe;

import java.util.Scanner;

public class ArithMain {

	public static void main(String[] args) {
		Arithmetic[] arith=new Arithmetic[4];
		Scanner sc=new Scanner(System.in);
		
		arith[0]=new Addition();
		arith[1]=new Subtraction();
		arith[2]=new Multiplication();
		arith[3]=new Division();
		System.out.println("MENU\n1.Addition\n2.Subtraction\n3.Multiplication\n4.Division");
		System.out.println("Enter the choice and two numbers");
		int ch=sc.nextInt();
		arith[ch-1].read(sc.nextInt(),sc.nextInt() );
		
		arith[ch-1].display();
		arith[ch-1].calculate();
	}

}

__________________________________________________________________________________________________________________________________________________________________________________________________________

TEST CASES:

package Javaexe;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ArithmeticTestcase {
	
	@Test
	void test() {
		Arithmetic ob = new Addition();
		int a=10;
		int b=20;
		int expected=30;
		ob.read(a, b);
		ob.calculate();
		assertEquals(expected, ob.num3);

	}
	@Test
	void test1() {
		Arithmetic ob = new Subtraction();
		int a=20;
		int b=10;
		int expected=10;
		ob.read(a, b);
		ob.calculate();
		assertEquals(expected, ob.num3);

	}
	@Test
	void test3() {
		Arithmetic ob = new Multiplication();
		int a=10;
		int b=2;
		int expected=20;
		ob.read(a, b);
		ob.calculate();
		assertEquals(expected, ob.num3);

	}
	@Test
	void test4() {
		Arithmetic ob = new Division();
		int a=20;
		int b=10;
		int expected=2;
		ob.read(a, b);
		ob.calculate();
		assertEquals(expected, ob.num3);

	}
}

