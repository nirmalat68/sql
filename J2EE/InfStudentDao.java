package web1;

import java.util.Map;

public interface InfStudentDao {

	public void getStudent(StudentVo obj) throws Exception;

	public Map<String, StudentVo> getStudent() throws Exception;
}
