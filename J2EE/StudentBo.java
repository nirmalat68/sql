package web1;

import java.util.Map;

public class StudentBo implements InfStudentBo {

	@Override
	public void getDetails(StudentVo obj) throws Exception {
		InfStudentDao infsdao = new StudentDao();
		infsdao.getStudent(obj);

	}
	@Override
	public Map<String, StudentVo> getDetails() throws Exception {
		InfStudentDao infsdao = new StudentDao();
		return infsdao.getStudent();

	}

}
