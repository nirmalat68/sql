package web2;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import web1.StudentBo;
import web1.StudentVo;

/**
 * Servlet implementation class SessionController
 */
public class SessionController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SessionController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StudentBo sbo;
		Map<String, StudentVo> map;
		RequestDispatcher rd;
		try {
			sbo = new StudentBo();
			map = sbo.getDetails();
			HttpSession session = request.getSession();
			session.setAttribute("mp", map);
			rd = request.getRequestDispatcher("DisplayAll.jsp");
			rd.forward(request, response);	
		} catch (Exception e) {
			
			request.setAttribute("msg", e.getMessage());
			rd = request.getRequestDispatcher("StudentError.jsp");
			rd.forward(request, response);	
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
